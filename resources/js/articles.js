import EditorJS from '@editorjs/editorjs';
import SimpleImage from './gif-images';
import {toJSON} from "lodash/seq";


const editor = new EditorJS({
    /**
     * Id of Element that should contain Editor instance
     */
    autofocus: true,

    holder: 'content-editorJs',

    tools: {
        GIF : SimpleImage
    },


});

document.getElementById('formContent').addEventListener('submit' , ele => {
    // ele.preventDefault();
    editor.save().then((outputData) => {
        document.getElementById('content').value = JSON.stringify(outputData);
        console.log('Article data: ', outputData)
    }).catch((error) => {
        console.log('Saving failed: ', error)
    });
});

var modal = document.getElementById("myModal");

var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}




