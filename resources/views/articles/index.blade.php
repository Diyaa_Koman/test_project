@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                @if(auth()->check())
                    <a href="{{route('articles.create')}}">
                        <buton class="btn btn-sm btn-primary" style="margin-bottom: 20px">Create new article</buton>
                    </a>
                @endif
                @foreach($articles as $article)
                    <div class="card" style="margin-bottom: 15px">
                        @if($loop->first)
                            <div class="card-header">Articles</div>
                        @endif

                        <div class="card-body">
                            <article>
                                <h2>{{$article->title}}</h2>
                                <input class="articles_index" value="{{$article->content}}" hidden>
                                <div class=""></div>
                                <p>{{$article->content}}</p>
                            </article>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection


