@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create New Article') }}</div>

                    <div class="card-body">
                        <form id="formContent" method="POST" action="{{ route('articles.store') }}" multiple="multipart/form-data">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label">{{ __('Title') }}</label>
                                <div class="col-md-12">
                                    <input id="title" type="text" class="form-control"
                                           name="title" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="content" class="col-md-4 col-form-label">{{ __('Content') }}</label>

                                <div class="col-md-12">
                                    <div id="content-editorJs" class="contentEditorJs"></div>
                                </div>
                            </div>
                            <input type="text" id="content" name="content" hidden>

                            <div class="row mb-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="col-md-5" style="margin-left: 27%;">
                <input class="form-control" placeholder="Search GIF ..." name="search" type="text" >
            </div>
            <div class="container row" style="margin-top: 25px">
                @foreach($gifs['data'] as $key => $gif)
                <div class="col-md-3 mb-3">
                    <img class="gif_list" id="one_gif_{{$key}}"
                          src="{{$gif['images']['original']['url']}}" style="width:220px;height:150px">
                </div>
                @endforeach
            </div>

        </div>

    </div>

@endsection

@push('styles')
    <link href="{{asset('css/gif-images.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/articles.css')}}" rel="stylesheet"/>
@endpush


@push('scripts')
    <script src="{{asset('js/gif-images.js')}}"></script>
    <script src="{{asset('js/articles.js')}}"></script>
@endpush
