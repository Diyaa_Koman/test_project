@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <button class="btn btn-sm btn-primary" style="margin-bottom: 20px">Craete new article</button>

            <div class="card" style="margin-bottom: 15px">
                <div class="card-header">Articles</div>
                <div class="card-body">
                    <article>
                        <h2>Google Chrome</h2>
                        <p>Google Chrome is a web browser developed by Google, released in 2008. Chrome is the world's most popular web browser today!</p>
                    </article>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
