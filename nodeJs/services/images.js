const http = require("../utils/http-provider");

class ImagesService{

   static getImages(limit,offset){
        return http.get(`/v1/gifs/trending`,{params:{api_key:process.env.GIFS_API_KEY,limit,offset}});
    }

}

module.exports = ImagesService;
