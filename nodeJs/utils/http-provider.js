const httpProvider = require("axios");
const {gifBaseUrl} = require("../constants");

http = httpProvider.create({
    baseURL: gifBaseUrl,
    timeout: 10000,
});

module.exports = http;
