const express = require("express")
const server = express()
const port = process.env.PORT ?? 3000
const routes = require('./routes')
const dotenv = require('dotenv');

dotenv.config({ path: './.env' });


server.use(express.json())
server.use('/api',routes)

server.listen(port,()=>{
    console.log(`listening on Port ${port}`)
})
