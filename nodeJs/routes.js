const express = require('express');
const ImagesService = require("./services/images");
const router = express.Router();
const Joi = require('joi');

router.get("/gifs",async (req,res)=>{

    const schema = Joi.object().keys({
       limit:Joi.number().integer().min(0),
        offset:Joi.number().integer().min(0),
    })
    const result = schema.validate(req.query);
    if(result.error){
       return res.status(400).send(result.error.message);
    }
    const response = await ImagesService.getImages(req.query.limit,req.query.offset)
    res.send(response.data)
})

module.exports = router;
