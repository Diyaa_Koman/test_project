<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\ArticleController::class, 'index'])->name('home');

Route::group(['prefix' => 'articles' , 'controller' => 'ArticleController'] , function () {
    Route::get('/', [App\Http\Controllers\ArticleController::class, 'index'])->name('articles.index');

    Route::group(['middleware' => 'web'] , function (){
        Route::get('/create', [App\Http\Controllers\ArticleController::class, 'create'])->name('articles.create');
        Route::post('/store', [App\Http\Controllers\ArticleController::class, 'store'])->name('articles.store');
    });
});
