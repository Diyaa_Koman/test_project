/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/gif-images.js":
/*!************************************!*\
  !*** ./resources/js/gif-images.js ***!
  \************************************/
/***/ ((module) => {

eval("function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== \"undefined\" && o[Symbol.iterator] || o[\"@@iterator\"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === \"number\") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError(\"Invalid attempt to iterate non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.\"); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it[\"return\"] != null) it[\"return\"](); } finally { if (didErr) throw err; } } }; }\n\nfunction _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === \"string\") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === \"Object\" && o.constructor) n = o.constructor.name; if (n === \"Map\" || n === \"Set\") return Array.from(o); if (n === \"Arguments\" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }\n\nfunction _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, \"prototype\", { writable: false }); return Constructor; }\n\nvar modal = document.getElementById(\"myModal\");\n\nvar gifImagesConst = /*#__PURE__*/function () {\n  function gifImages() {\n    _classCallCheck(this, gifImages);\n  }\n\n  _createClass(gifImages, [{\n    key: \"render\",\n    value: function render() {\n      var _this = this;\n\n      modal.style.display = \"block\";\n      this.wrapper = document.createElement('div');\n      this.wrapper.classList.add('simple-image');\n      var els = document.getElementsByClassName('gif_list');\n\n      var _iterator = _createForOfIteratorHelper(els),\n          _step;\n\n      try {\n        var _loop = function _loop() {\n          var el = _step.value;\n          el.addEventListener('click', function (event) {\n            _this.wrapper.appendChild(el);\n\n            modal.style.display = \"none\";\n          });\n        };\n\n        for (_iterator.s(); !(_step = _iterator.n()).done;) {\n          _loop();\n        }\n      } catch (err) {\n        _iterator.e(err);\n      } finally {\n        _iterator.f();\n      }\n\n      return this.wrapper;\n    }\n  }, {\n    key: \"save\",\n    value: function save(blockContent) {\n      return {\n        url: blockContent\n      };\n    }\n  }], [{\n    key: \"toolbox\",\n    get: function get() {\n      return {\n        title: 'GIF',\n        icon: '<svg width=\"17\" height=\"15\" viewBox=\"0 0 336 276\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z\"/></svg>'\n      };\n    }\n  }]);\n\n  return gifImages;\n}();\n\nmodule.exports = gifImagesConst;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJtb2RhbCIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJnaWZJbWFnZXNDb25zdCIsInN0eWxlIiwiZGlzcGxheSIsIndyYXBwZXIiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NMaXN0IiwiYWRkIiwiZWxzIiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsImVsIiwiYWRkRXZlbnRMaXN0ZW5lciIsImV2ZW50IiwiYXBwZW5kQ2hpbGQiLCJibG9ja0NvbnRlbnQiLCJ1cmwiLCJ0aXRsZSIsImljb24iLCJtb2R1bGUiLCJleHBvcnRzIl0sInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9naWYtaW1hZ2VzLmpzP2MyMWUiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIG1vZGFsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJteU1vZGFsXCIpO1xuXG5cbmNvbnN0IGdpZkltYWdlc0NvbnN0ID0gY2xhc3MgZ2lmSW1hZ2VzIHtcblxuICAgIHN0YXRpYyBnZXQgdG9vbGJveCgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHRpdGxlOiAnR0lGJyxcbiAgICAgICAgICAgIGljb246ICc8c3ZnIHdpZHRoPVwiMTdcIiBoZWlnaHQ9XCIxNVwiIHZpZXdCb3g9XCIwIDAgMzM2IDI3NlwiIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIj48cGF0aCBkPVwiTTI5MSAxNTBWNzljMC0xOS0xNS0zNC0zNC0zNEg3OWMtMTkgMC0zNCAxNS0zNCAzNHY0Mmw2Ny00NCA4MSA3MiA1Ni0yOSA0MiAzMHptMCA1MmwtNDMtMzAtNTYgMzAtODEtNjctNjYgMzl2MjNjMCAxOSAxNSAzNCAzNCAzNGgxNzhjMTcgMCAzMS0xMyAzNC0yOXpNNzkgMGgxNzhjNDQgMCA3OSAzNSA3OSA3OXYxMThjMCA0NC0zNSA3OS03OSA3OUg3OWMtNDQgMC03OS0zNS03OS03OVY3OUMwIDM1IDM1IDAgNzkgMHpcIi8+PC9zdmc+J1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIHJlbmRlcigpe1xuICAgICAgICBtb2RhbC5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuXG4gICAgICAgIHRoaXMud3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLndyYXBwZXIuY2xhc3NMaXN0LmFkZCgnc2ltcGxlLWltYWdlJyk7XG5cbiAgICAgICAgY29uc3QgZWxzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnZ2lmX2xpc3QnKTtcblxuICAgICAgICBmb3IoY29uc3QgZWwgb2YgZWxzKXtcbiAgICAgICAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy53cmFwcGVyLmFwcGVuZENoaWxkKGVsKTtcbiAgICAgICAgICAgICAgICBtb2RhbC5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLndyYXBwZXI7XG4gICAgfVxuXG5cbiAgICBzYXZlKGJsb2NrQ29udGVudCl7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB1cmw6IGJsb2NrQ29udGVudFxuICAgICAgICB9XG4gICAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdpZkltYWdlc0NvbnN0O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxJQUFJQSxLQUFLLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixTQUF4QixDQUFaOztBQUdBLElBQU1DLGNBQWM7RUFBQTtJQUFBO0VBQUE7O0VBQUE7SUFBQTtJQUFBLE9BU2hCLGtCQUFRO01BQUE7O01BQ0pILEtBQUssQ0FBQ0ksS0FBTixDQUFZQyxPQUFaLEdBQXNCLE9BQXRCO01BRUEsS0FBS0MsT0FBTCxHQUFlTCxRQUFRLENBQUNNLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtNQUNBLEtBQUtELE9BQUwsQ0FBYUUsU0FBYixDQUF1QkMsR0FBdkIsQ0FBMkIsY0FBM0I7TUFFQSxJQUFNQyxHQUFHLEdBQUdULFFBQVEsQ0FBQ1Usc0JBQVQsQ0FBZ0MsVUFBaEMsQ0FBWjs7TUFOSSwyQ0FRWUQsR0FSWjtNQUFBOztNQUFBO1FBQUE7VUFBQSxJQVFNRSxFQVJOO1VBU0FBLEVBQUUsQ0FBQ0MsZ0JBQUgsQ0FBb0IsT0FBcEIsRUFBNkIsVUFBQ0MsS0FBRCxFQUFXO1lBQ3BDLEtBQUksQ0FBQ1IsT0FBTCxDQUFhUyxXQUFiLENBQXlCSCxFQUF6Qjs7WUFDQVosS0FBSyxDQUFDSSxLQUFOLENBQVlDLE9BQVosR0FBc0IsTUFBdEI7VUFDSCxDQUhEO1FBVEE7O1FBUUosb0RBQW9CO1VBQUE7UUFLbkI7TUFiRztRQUFBO01BQUE7UUFBQTtNQUFBOztNQWVKLE9BQU8sS0FBS0MsT0FBWjtJQUNIO0VBekJlO0lBQUE7SUFBQSxPQTRCaEIsY0FBS1UsWUFBTCxFQUFrQjtNQUNkLE9BQU87UUFDSEMsR0FBRyxFQUFFRDtNQURGLENBQVA7SUFHSDtFQWhDZTtJQUFBO0lBQUEsS0FFaEIsZUFBcUI7TUFDakIsT0FBTztRQUNIRSxLQUFLLEVBQUUsS0FESjtRQUVIQyxJQUFJLEVBQUU7TUFGSCxDQUFQO0lBSUg7RUFQZTs7RUFBQTtBQUFBLEdBQXBCOztBQW1DQUMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCbEIsY0FBakIiLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvZ2lmLWltYWdlcy5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/gif-images.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./resources/js/gif-images.js");
/******/ 	
/******/ })()
;