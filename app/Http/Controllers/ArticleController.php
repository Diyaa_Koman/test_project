<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateArticleRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::query()->get();
        return view('articles.index' , compact('articles'));
    }

    public function create(){
        $gifs = Http::get('http://localhost:3000/api/gifs?limit=10');
        return view('articles.create', compact('gifs'));
    }

    public function store(CreateArticleRequest $request)
    {
        Article::query()->create($request->all());
        return redirect()->route('articles.index');
    }

}
