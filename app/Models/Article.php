<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title' , 'content'];

    protected static function booted()
    {
        static::creating(function ($article) {
            $article->creator_id = auth()->user()->id;
        });
    }

}
