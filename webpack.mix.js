const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/gif-images.js', 'public/js')
    .css('resources/css/gif-images.css', 'public/css')
    .js('resources/js/articles.js', 'public/js')
    .css('resources/css/articles.css', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
